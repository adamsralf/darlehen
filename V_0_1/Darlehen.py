# -*- coding: utf8 -*-
__version__ = '0.1'
__author__ = 'Ralf Adams'

"""Programm to compare different kind of loans."""

print ('***********************************************\n' 
       '**                                           **\n'
       '** Willkommen zu Darlehensrechner            **\n'
       '** Version: 0.1 vom 07.09.2017               **\n'
       '** Fehler bitte an: adams@tbs1.de            **\n'
       '**                                           **\n'
       '***********************************************\n')