# -*- coding: utf8 -*-
"""Program to compare different kind of loans."""

__version__ = '0.2'
__author__ = 'Ralf Adams (adams@tbs1.de)'

import time

cwidth = 80
first_last = ''.ljust(cwidth, '*')
padding = '**'.ljust(cwidth-2, ' ')+'**'
greeting1 = 'Willkommen zu Darlehensrechner'
greeting2 = 'Version: {v} vom {d}'.format(v=__version__, d=time.strftime("%d.%m.%Y"))
greeting3 = 'Fehler bitte an: {}'.format(__author__)
format_string = '**{{0:^{0}}}**'.format(cwidth-4)

print(first_last)
print(padding)
print(format_string.format(greeting1))
print(format_string.format(greeting2))
print(format_string.format(greeting3))
print(padding)
print(first_last)
