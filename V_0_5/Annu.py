# -*- coding: utf8 -*-
"""Program to compare different kind of loans."""

__version__ = '0.5'
__author__ = 'Ralf Adams (adams@tbs1.de)'

import time
from decimal import Decimal
import locale
import math

locale.setlocale(locale.LC_ALL, 'de-DE')

cwidth = 80
first_last = ''.ljust(cwidth, '*')
padding = '**'.ljust(cwidth-2, ' ')+'**'
greeting1 = 'Willkommen zur Berechnung des Annuitätendarlehns'
greeting2 = 'Version: {v} vom {d}'.format(v=__version__, d=time.strftime("%d.%m.%Y"))
greeting3 = 'Fehler bitte an: {}'.format(__author__)
format_string = '**{{0:^{0}}}**'.format(cwidth-4)

print(first_last)
print(padding)
print(format_string.format(greeting1))
print(format_string.format(greeting2))
print(format_string.format(greeting3))
print(padding)
print(first_last)

# Input and plausibility check of the loan
input_string = input('Bitte geben Sie die Kreditsumme in EUR an: ')
try:
    loan = Decimal(input_string)
    if loan <= 0:
        print('Die Eingabe "{0}" ist muss größer als 0 sein.'.format(input_string))
        exit()
except Exception:
    print ('Die Eingabe "{0}" ist keine Zahl.'.format(input_string))
    exit()

# Input and plausibility check of the rate
input_string = input('Bitte geben Sie den Zinssatz in % an:      ')
try:
    rate = Decimal(input_string)
    if rate <= 0:
        print('Die Eingabe "{0}" ist muss größer als 0 sein.'.format(input_string))
        exit()
    if rate > 10.0:
        print('Die Eingabe "{0}" ist beträgt mehr als 10%.'.format(input_string))
except Exception:
    print('Die Eingabe "{0}" ist keine Zahl.'.format(input_string))
    exit()
rate = rate/Decimal(100);

# Input and plausibility check of the period
input_string = input('Bitte geben Sie die Laufzeit in Jahren an: ')
try:
    period = Decimal(input_string)
    if period <= 0:
        print('Die Eingabe "{0}" ist muss größer als 0 sein.'.format(input_string))
        exit()
except Exception:
    print('Die Eingabe "{0}" ist keine Zahl.'.format(input_string))
    exit()

help1 = Decimal(1) + rate
help2 = help1 ** period
annual = loan * (help2 * rate) / (help2 - 1)

print('Die Annuität beträgt:                {0:>12}'.format(locale.currency((annual))))
print('Die Zinsen des Darlehens betragen:   {0:>12}'.format(locale.currency((annual * period - loan))))


year = 0
for year in range(1, int(period)+1):
    paying = (annual - loan * rate) * (help1 ** (year - 1))
    cheksum = cheksum + paying
    print('Die Tilgung im Jahr {0:>2} beträgt:      {1:>12}'.format(year, locale.currency((paying))))
    print('Die Zinsen im Jahr {0:>2} betragen:      {1:>12}'.format(year, locale.currency((annual - paying))))

