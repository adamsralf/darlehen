# -*- coding: utf8 -*-
"""Program to compare different kind of loans."""

__version__ = '0.3'
__author__ = 'Ralf Adams (adams@tbs1.de)'

import time
from decimal import Decimal
import locale

locale.setlocale(locale.LC_ALL, 'de-DE')

cwidth = 80
first_last = ''.ljust(cwidth, '*')
padding = '**'.ljust(cwidth-2, ' ')+'**'
greeting1 = 'Willkommen zu Darlehensrechner'
greeting2 = 'Version: {v} vom {d}'.format(v=__version__, d=time.strftime("%d.%m.%Y"))
greeting3 = 'Fehler bitte an: {}'.format(__author__)
format_string = '**{{0:^{0}}}**'.format(cwidth-4)

print(first_last)
print(padding)
print(format_string.format(greeting1))
print(format_string.format(greeting2))
print(format_string.format(greeting3))
print(padding)
print(first_last)

loan = Decimal(input('Bitte geben Sie den Darlehensbetrag in EUR an: '))
period = Decimal(input('Bitte geben Sie die Laufzeit in Jahren an:     '))
rate = Decimal(input('Bitte geben Sie den Zinssatz in % an:          '))

interest = loan * period * rate / Decimal(100);
total = loan + interest

print ('Die Zinsen des endfälligen Darlehens betragen: {0:>12}'.format(locale.currency((interest))))
print ('Die Gesamtfälligkeit ist                     : {0:>12}'.format(locale.currency(total)))
